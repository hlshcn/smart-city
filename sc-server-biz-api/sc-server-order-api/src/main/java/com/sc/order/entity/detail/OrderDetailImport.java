package com.sc.order.entity.detail;



/**
 * @author: wust
 * @date: 2020-07-15 14:16:53
 * @description:
 */
public class OrderDetailImport extends OrderDetail {
    // 行号，必须加
    private Integer row;

    // 是否成功，必须加
    private Boolean successFlag;

    // 错误原因，必须加
    private String errorMessage;

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    public Boolean getSuccessFlag() {
        return successFlag;
    }

    public void setSuccessFlag(Boolean successFlag) {
        this.successFlag = successFlag;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return super.toString() + "\nSysUserImport{" +
                "row=" + row +
                ", successFlag=" + successFlag +
                ", errorMessage='" + errorMessage + '\'' +
                '}';
    }
}