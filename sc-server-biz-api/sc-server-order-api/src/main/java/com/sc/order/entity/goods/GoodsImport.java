package com.sc.order.entity.goods;



/**
 * @author: wust
 * @date: 2020-07-15 13:54:06
 * @description:
 */
public class GoodsImport extends Goods {
    // 行号，必须加
    private Integer row;

    // 是否成功，必须加
    private Boolean successFlag;

    // 错误原因，必须加
    private String errorMessage;

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    public Boolean getSuccessFlag() {
        return successFlag;
    }

    public void setSuccessFlag(Boolean successFlag) {
        this.successFlag = successFlag;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return super.toString() + "\nSysUserImport{" +
                "row=" + row +
                ", successFlag=" + successFlag +
                ", errorMessage='" + errorMessage + '\'' +
                '}';
    }
}