package com.sc.order.entity.order;

import com.sc.common.entity.BaseEntity;

import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @author: wust
 * @date: 2020-07-15 14:14:39
 * @description:
 */
@Table(name = "t_order")
public class Order extends BaseEntity {
    private String orderNo;
    private Long buyerId;
    private String buyerName;
    private String buyerTelephone;
    private String buyerAddress;
    private BigDecimal totalAmount;
    private String status;
    private String payType;
    private String payStatus;
    private String businessSerialNumber;
    private Long projectId;
    private Long companyId;
    private Integer isDeleted;


    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setBuyerId(Long buyerId) {
        this.buyerId = buyerId;
    }

    public Long getBuyerId() {
        return buyerId;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerTelephone(String buyerTelephone) {
        this.buyerTelephone = buyerTelephone;
    }

    public String getBuyerTelephone() {
        return buyerTelephone;
    }

    public void setBuyerAddress(String buyerAddress) {
        this.buyerAddress = buyerAddress;
    }

    public String getBuyerAddress() {
        return buyerAddress;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus;
    }

    public String getPayStatus() {
        return payStatus;
    }

    public void setBusinessSerialNumber(String businessSerialNumber) {
        this.businessSerialNumber = businessSerialNumber;
    }

    public String getBusinessSerialNumber() {
        return businessSerialNumber;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

}