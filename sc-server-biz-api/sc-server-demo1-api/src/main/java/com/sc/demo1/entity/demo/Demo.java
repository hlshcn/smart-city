package com.sc.demo1.entity.demo;

import com.sc.common.annotations.EnableDistributedCaching;
import com.sc.common.annotations.EnableLocalCaching;
import com.sc.common.entity.BaseEntity;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.util.Date;

@EnableLocalCaching
@EnableDistributedCaching
public class Demo extends BaseEntity {
    private static final long serialVersionUID = 7668367640586322481L;

    /**
     * 网关名称
     */
    private String name;

    /**
     * 物理地址
     */
    private String mac;

    /**
     * 设备图片
     */
    private String image;

    /**
     * 设备ip
     */
    private String ip;

    /**
     * 设备序列号
     */
    private String sn;

    /**
     * 网关类型
     */
    private String type;

    /**
     * 软件版本号
     */
    @Column(name = "software_version")
    private String softwareVersion;

    /**
     * 硬件版本号
     */
    @Column(name = "hardware_version")
    private String hardwareVersion;

    /**
     * 网关类型
     */
    private String status;

    /**
     * 区域id
     */
    @Column(name = "area_id")
    private Long areaId;

    /**
     * 设备描述
     */
    private String description;

    /**
     * 定位
     */
    private String location;

    private BigDecimal pointX;

    private BigDecimal pointY;

    private Date lastConnectionTime;

    private Date lastDisconnectionTime;

    private Long projectId;

    private Long companyId;


    /**
     * 获取网关名称
     *
     * @return name - 网关名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置网关名称
     *
     * @param name 网关名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取物理地址
     *
     * @return mac - 物理地址
     */
    public String getMac() {
        return mac;
    }

    /**
     * 设置物理地址
     *
     * @param mac 物理地址
     */
    public void setMac(String mac) {
        this.mac = mac;
    }

    /**
     * 获取设备图片
     *
     * @return image - 设备图片
     */
    public String getImage() {
        return image;
    }

    /**
     * 设置设备图片
     *
     * @param image 设备图片
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * 获取设备ip
     *
     * @return ip - 设备ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * 设置设备ip
     *
     * @param ip 设备ip
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * 获取设备序列号
     *
     * @return sn - 设备序列号
     */
    public String getSn() {
        return sn;
    }

    /**
     * 设置设备序列号
     *
     * @param sn 设备序列号
     */
    public void setSn(String sn) {
        this.sn = sn;
    }

    /**
     * 获取网关类型
     *
     * @return type - 网关类型
     */
    public String getType() {
        return type;
    }

    /**
     * 设置网关类型
     *
     * @param type 网关类型
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 获取软件版本号
     *
     * @return software_version - 软件版本号
     */
    public String getSoftwareVersion() {
        return softwareVersion;
    }

    /**
     * 设置软件版本号
     *
     * @param softwareVersion 软件版本号
     */
    public void setSoftwareVersion(String softwareVersion) {
        this.softwareVersion = softwareVersion;
    }

    /**
     * 获取硬件版本号
     *
     * @return hardware_version - 硬件版本号
     */
    public String getHardwareVersion() {
        return hardwareVersion;
    }

    /**
     * 设置硬件版本号
     *
     * @param hardwareVersion 硬件版本号
     */
    public void setHardwareVersion(String hardwareVersion) {
        this.hardwareVersion = hardwareVersion;
    }

    /**
     * 获取网关类型
     *
     * @return status - 网关类型
     */
    public String getStatus() {
        return status;
    }

    /**
     * 设置网关类型
     *
     * @param status 网关类型
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 获取区域id
     *
     * @return area_id - 区域id
     */
    public Long getAreaId() {
        return areaId;
    }

    /**
     * 设置区域id
     *
     * @param areaId 区域id
     */
    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }

    /**
     * 获取设备描述
     *
     * @return description - 设备描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置设备描述
     *
     * @param description 设备描述
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 获取定位
     *
     * @return location - 定位
     */
    public String getLocation() {
        return location;
    }

    /**
     * 设置定位
     *
     * @param location 定位
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return point_x
     */
    public BigDecimal getPointX() {
        return pointX;
    }

    /**
     * @param pointX
     */
    public void setPointX(BigDecimal pointX) {
        this.pointX = pointX;
    }

    /**
     * @return point_y
     */
    public BigDecimal getPointY() {
        return pointY;
    }

    /**
     * @param pointY
     */
    public void setPointY(BigDecimal pointY) {
        this.pointY = pointY;
    }

    public Date getLastConnectionTime() {
        return lastConnectionTime;
    }

    public void setLastConnectionTime(Date lastConnectionTime) {
        this.lastConnectionTime = lastConnectionTime;
    }

    public Date getLastDisconnectionTime() {
        return lastDisconnectionTime;
    }

    public void setLastDisconnectionTime(Date lastDisconnectionTime) {
        this.lastDisconnectionTime = lastDisconnectionTime;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }
}