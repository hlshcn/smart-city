package com.sc.hystrix.config;

import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.context.annotation.Configuration;

@EnableCircuitBreaker
@Configuration
public class HystrixAutoConfiguration {
}
