package com.sc.easyexcel.definition;/**
 * Created by wust on 2017/5/8.
 */


import com.sc.easyexcel.ConfigDefinitionBean;

/**
 *
 * Function:注册配置接口
 * Reason:
 *
 * Date:2017/5/8
 * @author wust
 */
public interface ExcelDefinitionReader {
    /**
     * 注册配置
     * @return
     */
    ConfigDefinitionBean registerConfigDefinitions();
}
