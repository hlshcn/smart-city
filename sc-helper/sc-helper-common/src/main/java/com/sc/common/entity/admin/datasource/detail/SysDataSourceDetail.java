package com.sc.common.entity.admin.datasource.detail;

import com.sc.common.entity.BaseEntity;

/**
 * @author: wust
 * @date: 2020-04-13 16:23:14
 * @description:
 */
public class SysDataSourceDetail extends BaseEntity {
    private Long dataSourceId;
    private String actualTable;
    private String logicalTable;
    private String shardingColumn;

    public void setDataSourceId(Long dataSourceId) {
        this.dataSourceId = dataSourceId;
    }

    public Long getDataSourceId() {
        return dataSourceId;
    }

    public void setLogicalTable(String logicalTable) {
        this.logicalTable = logicalTable;
    }

    public String getLogicalTable() {
        return logicalTable;
    }

    public void setShardingColumn(String shardingColumn) {
        this.shardingColumn = shardingColumn;
    }

    public String getShardingColumn() {
        return shardingColumn;
    }

    public void setActualTable(String actualTable) {
        this.actualTable = actualTable;
    }

    public String getActualTable() {
        return actualTable;
    }

}