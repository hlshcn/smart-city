/**
 * Created by wust on 2019-10-30 09:45:28
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.common.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author: wust
 * @date: Created in 2019-10-30 09:45:28
 * @description: app专用api接口注解
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface AppApi {
}
