package com.sc.common.entity.admin.apptoken;

import com.sc.common.annotations.EnableDistributedCaching;
import com.sc.common.annotations.EnableLocalCaching;
import com.sc.common.entity.BaseEntity;

import java.util.Date;

@EnableLocalCaching
@EnableDistributedCaching
public class SysAppToken extends BaseEntity {

    private static final long serialVersionUID = -2133421096930010494L;

    private String appId;

    private Date appExpireTime;

    private String signSecretKey;

    private Long signDuration;

    private String applicant;

    private String status;

    private String description;

    private Long agentId;

    private Long parentCompanyId;

    private Long branchCompanyId;

    private Long projectId;


    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public Date getAppExpireTime() {
        return appExpireTime;
    }

    public void setAppExpireTime(Date appExpireTime) {
        this.appExpireTime = appExpireTime;
    }

    public String getSignSecretKey() {
        return signSecretKey;
    }

    public void setSignSecretKey(String signSecretKey) {
        this.signSecretKey = signSecretKey;
    }

    public Long getSignDuration() {
        return signDuration;
    }

    public void setSignDuration(Long signDuration) {
        this.signDuration = signDuration;
    }

    public String getApplicant() {
        return applicant;
    }

    public void setApplicant(String applicant) {
        this.applicant = applicant;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public Long getParentCompanyId() {
        return parentCompanyId;
    }

    public void setParentCompanyId(Long parentCompanyId) {
        this.parentCompanyId = parentCompanyId;
    }

    public Long getBranchCompanyId() {
        return branchCompanyId;
    }

    public void setBranchCompanyId(Long branchCompanyId) {
        this.branchCompanyId = branchCompanyId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }
}