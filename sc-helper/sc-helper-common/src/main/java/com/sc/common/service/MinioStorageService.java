package com.sc.common.service;

import com.sc.common.entity.admin.distributedfile.SysDistributedFile;
import java.io.InputStream;
import java.util.Map;

public interface MinioStorageService {
    SysDistributedFile upload(String bucketName, String objectName, InputStream stream, Long size, String contentType, String fileName, String fileType, String source);

    void delete(String bucketName, String objectName);

    void deleteDistributedFileByPrimaryKey(Long fileId);

    SysDistributedFile selectDistributedFileByPrimaryKey(Long fileId);
}
