package ${EntityPackageName};

import ${BasePackageName}.common.entity.BaseEntity;
import lombok.*;
import io.swagger.annotations.ApiModel;

/**
 * @author: ${Author}
 * @date: ${DateTime}
 * @description:
 */
 @ApiModel(description = "实体对象-${ClassName}")
 @NoArgsConstructor
 @AllArgsConstructor
 @ToString
 @Data
public class ${ClassName} extends BaseEntity {
    ${Properties}
}