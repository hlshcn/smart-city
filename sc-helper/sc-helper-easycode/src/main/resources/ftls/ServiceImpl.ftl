package ${ServicePackageName};

import ${DaoPackageName}.${ClassName}Mapper;
${InterfaceImport}
import ${EntityPackageName}.${ClassName};
import ${BasePackageName}.common.mapper.IBaseMapper;
import ${BasePackageName}.common.service.BaseServiceImpl;
import ${BasePackageName}.common.dto.WebResponseDto;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author: ${Author}
 * @date: ${DateTime}
 * @description:
 */
@Service("${EntityNameForCamelCase}ServiceImpl")
public class ${ClassName}ServiceImpl extends BaseServiceImpl<${ClassName}> implements ${ClassName}Service{
    @Autowired
    private ${ClassName}Mapper ${EntityNameForCamelCase}Mapper;

    @Override
    protected IBaseMapper getBaseMapper() {
        return ${EntityNameForCamelCase}Mapper;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }
}
