/**
 * Created by wust on 2020-03-30 10:44:32
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.demo1.core.cache;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import com.sc.common.annotations.EnableComplexCaching;
import com.sc.demo1.entity.demo.Demo;
import com.sc.common.enums.RedisKeyEnum;
import com.sc.common.cache.CacheAbstract;
import com.sc.common.util.cache.SpringRedisTools;
import com.sc.demo1.core.dao.DemoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author: wust
 * @date: Created in 2020-03-30 10:44:32
 * @description: 网关缓存
 *
 */
@EnableComplexCaching(dependsOnPOSimpleName = "Demo")
@Component
public class RedisCacheDemoBo extends CacheAbstract {

    @Autowired
    private SpringRedisTools springRedisTools;

    @Autowired
    private DemoMapper demoMapper;

    @Autowired
    private Environment environment;

    @Override
    public void init() {
        Set<String> keys = springRedisTools.keys(RedisKeyEnum.REDIS_KEY_HASH_GROUP_GATEWAY_BY_MAC.getStringValue().replaceAll("%s_","*"));
        if(keys != null && keys.size() > 0){
            springRedisTools.deleteByKey(keys);
        }

        List<Demo> demos =  demoMapper.selectAll();
        for (Demo demo : demos) {
            cacheByMac(demo);
        }
    }

    @Override
    public void reset() {

    }

    @Override
    public void add(Object obj){
        if(obj == null){
            return;
        }

        Demo entity = null;

        if(obj instanceof Long){
            entity = demoMapper.selectByPrimaryKey(obj);
        }else if(obj instanceof Demo){
            entity = (Demo) obj;
        }else if(obj instanceof Map){
            entity = JSONObject.parseObject(JSONObject.toJSONString(obj),Demo.class);
        }

        if(entity == null){
            return;
        }

        cacheByMac(entity);
    }

    @Override
    public void batchAdd(List<Object> list){
        if(CollectionUtil.isNotEmpty(list)){
            for (Object o : list) {
                add(o);
            }
        }
    }

    @Override
    public void updateByPrimaryKey(Object primaryKey){
        Demo gateway =  demoMapper.selectByPrimaryKey(primaryKey);
        if(gateway != null){
            cacheByMac(gateway);
        }
    }

    @Override
    public void batchUpdate(List<Object> list){
        if(CollectionUtil.isNotEmpty(list)){
            for (Object o : list) {
                updateByPrimaryKey(o);
            }
        }
    }

    @Override
    public void deleteByPrimaryKey(Object primaryKey){
        Demo gateway =  demoMapper.selectByPrimaryKey(primaryKey);
        if(gateway != null){
            String key1 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_GATEWAY_BY_MAC.getStringValue(),gateway.getMac());
            if(springRedisTools.hasKey(key1)){
                springRedisTools.deleteByKey(key1);
            }
        }
    }

    @Override
    public void batchDelete(List<Object> primaryKeys){
        if(CollectionUtil.isNotEmpty(primaryKeys)){
            for (Object primaryKey : primaryKeys) {
                deleteByPrimaryKey(primaryKey);
            }
        }
    }


    private void cacheByMac(Demo gateway){
        if(gateway == null){
            return;
        }

        String key1 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_GATEWAY_BY_MAC.getStringValue(),gateway.getMac());
        if(springRedisTools.hasKey(key1)){
            springRedisTools.deleteByKey(key1);
        }
        Map mapValue1 = BeanUtil.beanToMap(gateway);
        springRedisTools.addMap(key1,mapValue1);
    }
}
