package com.sc.demo1.common.aspect;


import cn.hutool.core.util.ReflectUtil;
import com.sc.common.aspect.BaseAspect;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

/**
 * 当操作数据库时，处理分片列
 * 比如，更新操作，如果发现分片列被更新，则使用反射把更新列的值置为空
 */
@Aspect
@Component
public class ShardingColumnAspect extends BaseAspect {
    @Pointcut("execution(* com.sc.common.mapper.IBaseMapper.updateByPrimaryKeySelective(..))")
    private void updateByPrimaryKeySelective(){}


    @Around("updateByPrimaryKeySelective()")
    public Object around(ProceedingJoinPoint jp) throws Throwable {
        Object[] args = jp.getArgs();
        Signature sig = jp.getSignature();
        if (sig instanceof MethodSignature) {
            Object obj = args[0];

            Object valueObj = ReflectUtil.getFieldValue(obj,"companyId");
            if(valueObj != null){
                ReflectUtil.setFieldValue(obj,"companyId",null);
            }
        }
        return jp.proceed();
    }

}
