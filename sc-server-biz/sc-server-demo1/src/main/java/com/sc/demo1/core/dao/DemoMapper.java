package com.sc.demo1.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.demo1.entity.demo.Demo;

public interface DemoMapper extends IBaseMapper<Demo> {
}