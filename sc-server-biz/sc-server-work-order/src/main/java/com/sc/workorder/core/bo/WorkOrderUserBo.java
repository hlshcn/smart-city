/**
 * Created by wust on 2020-04-09 10:14:08
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.workorder.core.bo;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.entity.admin.user.SysUser;
import com.sc.workorder.entity.WoWorkOrderUser;
import com.sc.common.util.cache.DataDictionaryUtil;
import com.sc.workorder.core.dao.WoWorkOrderUserMapper;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;
import java.util.List;

/**
 * @author: wust
 * @date: Created in 2020-04-09 10:14:08
 * @description:
 *
 */
@Component
public class WorkOrderUserBo {
    @Autowired
    private WoWorkOrderUserMapper woWorkOrderUserMapper;

    public JSONArray buildCascader(Long workOrderTypeId){
        final JSONArray jsonArray = new JSONArray();

        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        Example example = new Example(WoWorkOrderUser.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("projectId",ctx.getProjectId());
        criteria.andEqualTo("companyId",ctx.getBranchCompanyId());
        criteria.andEqualTo("workOrderTypeId",workOrderTypeId);
        List<WoWorkOrderUser> woWorkOrderUsers =  woWorkOrderUserMapper.selectByExample(example);
        if(CollectionUtils.isNotEmpty(woWorkOrderUsers)){
            for (WoWorkOrderUser woWorkOrderUser : woWorkOrderUsers) {
                SysUser user = DataDictionaryUtil.getApplicationPOByPrimaryKey(woWorkOrderUser.getUserId(),SysUser.class);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("value",user.getId());
                jsonObject.put("label",user.getLoginName() + "(" +user.getRealName() + ")");
                jsonArray.add(jsonObject);
            }
        }
        return jsonArray;
    }
}
