package com.sc.admin.core.service;


import com.sc.common.entity.admin.datasource.detail.SysDataSourceDetail;
import com.sc.common.service.BaseService;

/**
 * @author: wust
 * @date: 2020-05-01 20:32:48
 * @description:
 */
public interface SysDataSourceDetailService extends BaseService<SysDataSourceDetail> {
}
