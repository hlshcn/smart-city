/**
 * Created by wust on 2020-03-31 09:13:01
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.admin.core.service.impl;


import com.sc.admin.core.service.SysOrganizationService;
import com.sc.common.service.InitializtionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

/**
 * @author: wust
 * @date: Created in 2020-03-31 09:13:01
 * @description:
 *
 */
@Order(2)
@Service
public class InitData4OrganizationServiceImpl implements InitializtionService {
    @Autowired
    private SysOrganizationService sysOrganizationServiceImpl;

    @Override
    public void init() {
        sysOrganizationServiceImpl.initDefaultOrganization();
    }
}
