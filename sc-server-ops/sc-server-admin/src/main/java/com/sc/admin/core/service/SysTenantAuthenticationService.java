package com.sc.admin.core.service;

import com.sc.admin.entity.tenant.SysTenantAuthentication;
import com.sc.common.service.BaseService;

/**
 * @author: wust
 * @date: 2020-12-03 10:21:44
 * @description:
 */
public interface SysTenantAuthenticationService extends BaseService<SysTenantAuthentication>{
}
