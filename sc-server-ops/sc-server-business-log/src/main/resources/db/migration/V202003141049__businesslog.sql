CREATE TABLE IF NOT EXISTS `flyway_schema_history` (
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `description` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_general_ci NOT NULL,
  `script` varchar(1000) COLLATE utf8mb4_general_ci NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`installed_rank`),
  KEY `flyway_schema_history_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



CREATE TABLE IF NOT EXISTS `bl_operation_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `business_name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `operation_role` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `operation_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `operation_type` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `operation_ip` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '来源',
  `creater_id` bigint(20) DEFAULT NULL,
  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `modify_id` bigint(20) DEFAULT NULL,
  `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `modify_time` datetime DEFAULT NULL,
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;



-- 从当前月开始，创建两年的表 S
DELIMITER //
CREATE PROCEDURE createOperationLogTablesWithMonth(beginYear INT(11), endYear INT(11))
BEGIN
    DECLARE `@yBegin` INT(11);
    DECLARE `@yEnd` INT(11);
    DECLARE `@mBegin` INT(11);
    DECLARE `@mEnd` INT(11);
    DECLARE `@createSql` VARCHAR(2560);
    DECLARE `@t` VARCHAR(50);
    SET `@yBegin`= beginYear;
    SET `@yEnd`= endYear;
    SET `@t`='';
    WHILE `@yBegin`<= `@yEnd` DO
      	SELECT `@yBegin`;
      	SET `@mBegin`= 1;
    		SET `@mEnd`= 12;
 	 		WHILE `@mBegin`<= `@mEnd` DO
    	  		SELECT `@mBegin`;
        		SET `@t` = CONCAT(`@yBegin`,LPAD(`@mBegin`,2,'0'));
        		SELECT `@t`;
        		SET @createSql = CONCAT('CREATE TABLE IF NOT EXISTS bl_operation_log_',`@t`,"(
	                                  `id` bigint(20) NOT NULL AUTO_INCREMENT,
	                                  `module_name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
	                                  `business_name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
	                                  `operation_role` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
	                                  `operation_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
	                                  `operation_type` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
	                                  `operation_ip` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
	                                  `source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '来源',
	                                  `creater_id` bigint(20) DEFAULT NULL,
	                                  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
	                                  `create_time` datetime DEFAULT NULL,
	                                  `modify_id` bigint(20) DEFAULT NULL,
	                                  `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
	                                  `modify_time` datetime DEFAULT NULL,
	                                  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
	                                   PRIMARY KEY (`id`) USING BTREE
	                                   ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
	        	");
		    	PREPARE createSql FROM @createSql;
		    	EXECUTE createSql;
		    	SET `@mBegin`= `@mBegin` + 1;
			END WHILE;
			SET `@yBegin`= `@yBegin` + 1;
    END WHILE;
END //
DELIMITER;
-- 从当前月开始，创建两年的表 E