package com.sc.businesslog.core.mq.consumer;

import com.alibaba.fastjson.JSONObject;
import com.sc.businesslog.core.service.BlOperationLogService;
import com.sc.businesslog.entity.BlOperationLog;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * @author ：wust
 * @date ：Created in 2019/8/9 10:13
 * @description：
 * @version:
 */
@Component
@RabbitListener(
        containerFactory = "singleListenerContainer",
        bindings = @QueueBinding(
                value = @Queue(
                        value = "${spring.rabbitmq.operationlog.queue.name}",
                        durable = "${spring.rabbitmq.operationlog.queue.durable}"
                ),
                exchange = @Exchange(
                        value = "${spring.rabbitmq.operationlog.exchange.name}",
                        durable = "${spring.rabbitmq.operationlog.exchange.durable}",
                        type = "${spring.rabbitmq.operationlog.exchange.type}",
                        ignoreDeclarationExceptions = "${spring.rabbitmq.operationlog.exchange.ignoreDeclarationExceptions}"
                ),
                key = "${spring.rabbitmq.operationlog.routing-key}"
        )
)
public class Consumer4OperationLogQueue {

    @Autowired
    private BlOperationLogService blOperationLogServiceImpl;

    @RabbitHandler
    public void process(JSONObject jsonObject) {
        BlOperationLog sysOperationLog = jsonObject.toJavaObject(BlOperationLog.class);
        sysOperationLog.setIsDeleted(0);
        blOperationLogServiceImpl.insert(sysOperationLog);
    }
}
