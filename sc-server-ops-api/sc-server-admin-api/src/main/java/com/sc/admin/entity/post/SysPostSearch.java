package com.sc.admin.entity.post;

import com.sc.common.dto.PageDto;
import lombok.*;
import io.swagger.annotations.ApiModel;

/**
 * @author: wust
 * @date: 2020-12-03 11:14:47
 * @description:
 */
 @ApiModel(description = "查询对象-SysPostSearch")
 @NoArgsConstructor
 @AllArgsConstructor
 @ToString
 @Data
public class SysPostSearch extends SysPost {
    private PageDto pageDto;
}